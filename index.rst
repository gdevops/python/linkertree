
.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


|FluxWeb| `RSS <https://gdevops.frama.io/python/linkertree/rss.xml>`_

.. _python_linkertree:

=======================
**Python linkertree**
=======================

- https://rstockm.github.io/mastowall/?hashtags=python&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=python&server=https://fosstodon.org

- https://fedidevs.com/python/
- https://fedidevs.com/starter-packs/?q=python 


- https://gdevops.frama.io/python/versions/
- https://gdevops.frama.io/python/news-2025
- https://gdevops.frama.io/python/news-2024
- https://gdevops.frama.io/python/news-2023
- https://gdevops.frama.io/python/news/
- https://gdevops.frama.io/python/tuto
